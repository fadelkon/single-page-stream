

_**PROJECT MOVED TO [0xACAB.org](https://0xacab.org/autonomoustream/single-page-stream)**_


# ~~Single page streaming~~

This is project grows on top of https://live.autistici.org/

## Goal

A simple web frontend to the Autistici stream server with added, 3d party chat.

## Demo

* Self hosted: https://sub.marin.li/media/stream.html#some-channel
* Not working, gitlab pages: https://fadelkon.frama.io/single-page-stream/stream.html#some-channel

## About the video

It uses video.js and it's hardcoded to play streams from autistici's server.

From the original frontend:
> To create a new stream, pick a unique name for it, and set your broadcast software to stream to rtmp://live.autistici.org/ingest/name.
> You will then be able to watch the live stream at https://live.autistici.org/#name.


## About the chat

[Cryptodog](https://github.com/Cryptodog/cryptodog) is a "static" web chat that works well. It uses its own XMPP servers, encrypts
the content with OTR but can't encrypt both channel/room name nor usernames,
nor other more obvious metadata as timestamps. Just open the network tab in your browser's dev tools (F12) to check it.

As a user, enter a username and a room name, and you will join a XMPP conference room
with this name, where you'll find other users that have joined it too.


## Install

In a webser-accessible folder, do:
```
# clone this repo
git clone https://framagit.org/fadelkon/single-page-stream

# clone the depended repo, cryptodog's
git submodule update chat --init
```
And access to `/stream.html`

## Credit

Please [contribute to Autistici-Inventati](https://www.autistici.org/donate) who voluntarily develop and pay for the
expensive stream servers and many other network applications esssential for activists.
